//let h1 = document.querySelector('h1');
//
//let text = h1.textContent;
//
//h1.textContent='';

// nukirpti teksta naudojant cikla

//let newText = '';
//
//for (let i=0; i<30; i++) {
//    newText+=text[i];
//}
//newText+='...';
//
//h1.textContent=newText;

// naudojant splice

//let h2 = document.querySelector('h2');
//let h2Text = h2.textContent;

// TO DO LISTAS

const tasks = document.querySelectorAll('.task input');
const activeDay = document.querySelectorAll('tr');
const day = document.querySelectorAll('.day');
const done = document.querySelectorAll('.done');

let toDoList = [];
let toDoHistory = JSON.parse(localStorage.getItem('toDo'));
tasks.forEach((task, i) => {
    // paimam objekta is localstorage
    // jei objekto nera, tai toDoHistory bus undefined ir salyga nebus vykdoma
    if (toDoHistory) {
        console.log(toDoHistory);
        task.value = toDoHistory[i].toDo;
        done[i].textContent = toDoHistory[i].done;
        if (toDoHistory[i].done === 'v') {
            done[i].style.color = 'green';
        }
        
        toDoList.push(toDoHistory[i]);
    } else {
        console.log('local does not exist');
        toDoList.push({
            day: day[i].textContent,
            toDo: '',
            done: 'x'
        })
    }

    // uzdedam eventlisteneri ant inputu
    task.addEventListener('input', function () {
        // irasom property toDo su inputo verte
        toDoList[i].toDo = tasks[i].value;
        // setinam jsona i local storaga
        localStorage.setItem('toDo', JSON.stringify(toDoList));
    })
})

console.log(toDoList)
console.log(toDoList)
//let arr = ['x', 'v']

done.forEach((item, i) => {
    item.addEventListener('click', function () {
        item.textContent = 'v';
        item.style.color = 'green';
        toDoList[i].done = 'v';
        localStorage.setItem('toDo', JSON.stringify(toDoList));
        console.log(toDoList)
    })
})

let now = new Date();
let weekDay = now.getDay() - 1;

activeDay[weekDay].style.backgroundColor = 'azure';
tasks[weekDay].style.backgroundColor = 'azure';

localStorage.setItem('key', 'poop');

console.log('-----------');

const box = document.querySelector('.box');
let hist = localStorage.getItem('txt');
if (hist == null) {
    hist = '';
}

box.value = hist;
box.addEventListener('input', function () {
    localStorage.setItem('txt', box.value);
})
