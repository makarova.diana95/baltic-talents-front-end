document.querySelector('.logo').style.backgroundColor = 'rgb(175,175,175)';
document.querySelector('.main_title h2').style.color = 'rgb(175,175,175)';
document.querySelector('.landing_page .circle').style.border = '1px solid rgb(229,230,235)';
document.querySelector('.landing_page .scroll').style.color = 'rgb(229,230,235)';
document.querySelector('.main_title h1').style.color = 'rgb(229,230,235)';
document.querySelector('.awesome').style.backgroundColor = 'rgb(175,175,175)';
document.querySelector('.awesome .section_title').style.color = 'rgba(229,230,235,.5)';
document.querySelector('.awesome .section_title span').style.letterSpacing = '5px';

const list = document.querySelectorAll('li a');
const bubbles = document.querySelectorAll('.wrapper .bubble')

document.querySelector('.main_title h1').addEventListener('click', function() {
    document.querySelector('.main_title h1').style.color='rgba(229,230,235,.5)'
})

for (let i = 0; i < bubbles.length; i++) {
    bubbles[i].style.backgroundColor='rgba(229,230,235,.5)';
}

for (let i = 0; i < list.length; i++) {
    list[i].addEventListener('mouseover', function() {
        list[i].style.borderBottom='4px solid rgb(175,175,175)';
    })
    
    list[i].addEventListener('mouseleave', function() {
        list[i].style.borderBottom='none';
    })
}

document.querySelector('.landing_page .circle').addEventListener('mouseover', function () {
    document.querySelector('.landing_page .circle').style.pointer='cursor';
})

var popup = document.querySelector('.popup');
var awesome = document.querySelector('.awesome');

window.addEventListener('scroll', function() {
//    console.log(window.scrollY, window.innerHeight, popup.offsetTop/2, awesome.offsetTop, 150);
//
//    console.log(window.scrollY + window.innerHeight, popup.offsetTop/2 + awesome.offsetTop + 150);
    
    if (window.scrollY + window.innerHeight > popup.offsetTop + awesome.offsetTop + 150) {
        popup.style.transition='.2s';
        popup.style.opacity='1';
    }
    
})

var closeBtn = document.querySelector('.close_btn');
//console.log(closeBtn);
closeBtn.addEventListener('click', function() {
//    console.log('click');
    popup.style.display='none';
})