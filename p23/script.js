console.log('Objektai');

// Konstruktorius

// Brezinys
function Player(name, surname, dateOfBirth, totalGoals, matchesPlayed) {
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
    this.totalGoals = totalGoals;
    this.matchesPlayed = matchesPlayed;
    this.getAge = function(currentDate) {
        console.log("Player's age: " + (currentDate - this.dateOfBirth));
    };
    this.getInfo = function() {
        console.log(this.name, this.surname);
    };
    this.getAvgGoalsPerMatch = function() {
        const avgGoals = this.totalGoals/this.matchesPlayed;
        console.log('Average goals per match: ' + avgGoals);
        return avgGoals;
    };
    this.compareAge = function(other) {
        if (this.dateOfBirth>other.dateOfBirth) {
            let info = `Player ${this.name} is younger than ${other.name}`;
            console.log(info);
        } else if (this.dateOfBirth<other.dateOfBirth) {
            let info = `Player ${this.name} is older than ${other.name}`;
            console.log(info);
        }
    };
    this.compareAvgGoals = function(other) {
        if (this.getAvgGoalsPerMatch()>other.getAvgGoalsPerMatch()) {
            let info = `Player ${this.name} is better`;
            console.log(info);
        } else if (this.getAvgGoalsPerMatch()<other.getAvgGoalsPerMatch()) {
            let info = `Player ${other.name} is better`;
            console.log(info);
        }
    }
}

// Kuriam objekta pagal brezini
let player1 = new Player('Jonas', 'Jonaitis', 1998, 13, 20);
let player2 = new Player('Antanas', 'Antanaitis', 1993, 10, 12);

// Palyginam zaideju amziu ir kuris geresnis pagal ivarciu sk
player1.compareAge(player2);
player1.compareAvgGoals(player2);

const date = new Date();
const year = date.getFullYear();
// date.__proto__   <- gauti visus metodus

// Gaunam zaidejo info
player1.getInfo();
player2.getInfo();

console.log('--------------------');

// Gaunam zaidejo vid ivarciu sk
player1.getAvgGoalsPerMatch();
player2.getAvgGoalsPerMatch();

// Gaunam zaidejo amziu
player1.getAge(year);

player1.dis = true;



// setTimeout() palogina tik viena karta, setInterval() logina kas kazkiek laiko

//setTimeout(function() {
//    
//}, 2000)

setTimeout(()=>{
    console.log('--------------------');
    console.log('Intervalai');
//    console.log(Math.floor(Math.random()*100));
}, 500)

//let i = 1;
//let counter = setInterval(()=>{
//    console.log(i++);
//    if (i == 16) {
//        clearInterval(counter);
//    }
//}, 400)

// clearInterval() sustabdo intervala

let h1 = document.querySelector('h1');
let text = h1.textContent;
h1.textContent='';

let j = 0;
let printer = setInterval(()=>{
    h1.textContent+=text[j];
    j++;
    if (j>=text.length) {
        clearInterval(printer);
    }
}, 200)

// sukurti konstruktoriu Shape
// turi tureti tipa: staciakampis arba statusis trikampis; krastine a; krastine b
// metodas: gauti plota; perimetra
// palyginti tu dvieju figuru plota
// padaryti rulete 0-36 sk

console.log('--------------------');

function Shape(type, sideA, sideB) {
    this.type = type;
    this.sideA = sideA;
    this.sideB = sideB;
    this.getArea = function() {
        if (this.type == 'rectangle') {
            let area = this.sideA*this.sideB;
            return area;
        } else if (this.type == 'triangle') {
            let area = this.sideA*this.sideB/2;
            return area;
        }
    };
    this.getPerimeter = function() {
        if (this.type == 'rectangle') {
            let per = (this.sideA+this.sideB)*2;
            console.log("Rectangle's perimeter is: " + per);
        } else if (this.type == 'triangle') {
            let sideC = Math.sqrt(this.sideA*this.sideA + this.sideB*this.sideB);
            let per = this.sideA+this.sideB+sideC;
            console.log("Triangle's perimeter is: " + (this.sideA+this.sideB+sideC).toFixed(2));
        }
    };
    this.compareArea = function(other) {
        let areaA = this.getArea();
        let areaB = other.getArea();
        if (areaA > areaB) {
            let comparison = `Shape ${this.type}'s area ${areaA} is bigger than ${other.type}'s area ${areaB}`;
            console.log(comparison);
        } else if (areaA < areaB) {
            let comparison = `Shape ${other.type}'s area ${areaB} is bigger than ${this.type}'s ${areaA}`;
            console.log(comparison);
        } else {
            let comparison = `Shapes ${this.type} and ${other.type} have the same area of ${areaA}`;
            console.log(comparison);
        }
    }
}

let shape1 = new Shape('rectangle', 13, 10);
let shape2 = new Shape('triangle', 8, 10);

//shape1.compareArea(shape2);

console.log('--------------------');


let h2 = document.querySelector('h2');
let input = document.querySelector('input');
let btn = document.querySelector('button');

input.focus();
btn.addEventListener('click', function() {
    
    input.focus();
    btn.textContent='Roll again';
    
    if (input.value == '') {
        return;
    }
    
    let i = 0;
    let counter = setInterval(()=>{
        let num = Math.floor(Math.random()*100+1);
        h2.textContent = num;
        i++;
        if (i >= 25) {
            clearInterval(counter);
            h2.textContent=num;
            console.log('input was: ' + input.value, 'number was: ' + num);
            if (input.value == num) {
                h2.style.color='green';
            } else {
                h2.style.color='red';
            }
        }
    }, 100)
})

//setInterval(()=>{
//    console.log(i++);
//    if (i == 16) {
//        clearInterval(counter);
//    }
//}, 100)

// sukurti 3-6 figuras
// konstruktoriuje aprasyti metoda, kuris playgina pasirinktos figuros plota su kitomis figuromis