// DOM BOM Events

// #1 pagal id senas budas
const heading = document.getElementById('heading');

// #2 randam viena elementa pagal pasirinktas savybes
const btn = document.querySelector('button');
const title = document.querySelector('.list');

// #3 surenkam visus norimus elementus
const list = document.querySelectorAll('li');

// modifikuojam elementu savybes

    // stilius
    heading.style.backgroundColor='salmon';
    title.style.borderBottom='1px solid black';

    // pakeisti teksta
    heading.textContent='JavaScript';
    title.innerHTML='<span>Activities</span>'; // bet negeras budas

    list[2].style.fontWeight='600';

    for (let i = 0; i < list.length; i++) {
        list[i].style.color='blue';
    }

// events
btn.addEventListener('click', changeHeader);

function changeHeader() {
    
    if (heading.style.backgroundColor=='salmon') {
        heading.style.backgroundColor='green';
    } else {
        heading.style.backgroundColor='salmon';
    }
    
}

// paduodam anonimine funkcija
title.addEventListener('click', function() {
    title.style.backgroundColor='orange';
})
