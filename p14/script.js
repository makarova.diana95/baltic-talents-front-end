console.log('JavaScript')

//kintamuju tipai:
//
//String
//Number
//boolean
//undefined
//object
//null

//kintamuju deklaravimas
//kintamaji deklaruojam tik viena karta
var word = 'JavaScript';
var nr = 10;
console.log(word,nr)

var truth = true;
var lie = false;
console.log(truth,lie)

var nerastas;
console.log(nerastas)
nerastas = 0;
console.log(nerastas)

var niekas = null;
console.log(niekas)

// ES6 sintakse 
let paragraph = 'la la la';
const p = 3.14;
//p = 0;
//negalime priskirt naujos reiksmes ^^^
// baigiam ES6

// kintamuju mutacijos
var myName = 'Diana'
var myAge = 22;
var info = 'My name is ' + myName + ' and I am ' + myAge;
let infoES6 = `My name is ${myName} and I am ${myAge}` 

console.log(info)
console.log(infoES6)

// staciakampio trikampio ploto skaiciavimas 
var krastineA = 9;
var krastineB = 6;
var plotas = krastineA * krastineB / 2;
console.log(`Trikampio plotas yra ${plotas}`)

// math operators (0-1)
var apvalintas = Math.floor(Math.random()*10)
console.log(apvalintas)

// salygos
// >
// <
// >=
// <=
// == lygu tikrina tik verte
// === labai lygu lygina ir verte ir tipa
// ! ne
// != nelygu
// !== labai nelygu


var no1 = '20';
var no2 = 20;

if (no1 > no2) {
    console.log(`${no1} is more than ${no2}`);
} else if (no1 < no2) {
    console.log(`${no1} is less than ${no2}`);
} else if (no1 === no2) {
    console.log(`${no1} and ${no2} are equal`);
} else {
    console.error(`Error`);
    console.warn(`Warning`);
}

// deklaruoti dvieju staciakampiu krastines a ir b su jusu pasirinktu dydziu
// paskaiciuot istaciakampiu  plota ir perimetra
// palyginti ju plotus
// palyginti perimetrus

// sugeneruoti staciakampi su dviem atsitiktinio dydzio krastinem
// paskaiciuoti to staciakampio plota ir peirimetra

// tas pats tik su trikampiu

// #hardcore sugeneruoti du staciakampius su atsitiktinem krastinem ir salygoje palyginti ju plotus ir perimetrus

var stacAkrastineA = 9;
var stacAkrastineB = 14;
var stacAperimetras = (stacAkrastineA + stacAkrastineB) * 2; // 46
var stacAplotas = stacAkrastineA * stacAkrastineB; // 126
console.log(stacAperimetras, stacAplotas);

var stacBkrastineA = 7;
var stacBkrastineB = 11;
var stacBperimetras = (stacBkrastineA + stacBkrastineB) * 2; // 36
var stacBplotas = stacBkrastineA * stacBkrastineB; // 77
console.log(stacBperimetras, stacBplotas);

if (stacAperimetras > stacBperimetras) {
    console.log(`staciakampio a perimetras ${stacAperimetras} > uz staciakampio b perimetra ${stacBperimetras}`)
} else if (stacAperimetras < stacBperimetras) {
    console.log(`staciakampio a perimetras ${stacAperimetras} < uz staciakampio b perimetra ${stacBperimetras}`)
} else if (stacAperimetras === stacBperimetras) {
    console.log(`staciakampio a ir b perimetrai ${stacAperimetras} yra lygus`)
} else {
    console.error(`Error`)
}

if (stacAplotas > stacBplotas) {
    console.log(`staciakampio a plotas ${stacAplotas} > uz staciakampio b plota ${stacBplotas}`)
} else if (stacAplotas < stacBplotas) {
    console.log(`staciakampio a plotas ${stacAplotas} < uz staciakampio b plota ${stacBplotas}`)
} else if (stacAplotas === stacBplotas) {
    console.log(`staciakampio a ir b plotai ${stacAplotas} yra lygus`)
} else {
    console.error(`Error`)
}

var stacCkrastineA = Math.floor(Math.random() * (10 - 1)) + 1;
var stacCkrastineB = Math.floor(Math.random() * (10 - 1)) + 1;
console.log(stacCkrastineA, stacCkrastineB);
var stacCperimetras = (stacCkrastineA + stacCkrastineB) * 2;
var stacCplotas = stacCkrastineA * stacCkrastineB;
console.log(stacCperimetras, stacCplotas);

