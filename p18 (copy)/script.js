// DOM BOM Events

// kintamuju nuemimas

const input = document.querySelector('input');
const guess = document.querySelector('.guess');
const startOver = document.querySelector('.start_over');
const info = document.querySelector('p');
const h1 =  document.querySelector('h1');

let rand = Math.floor(Math.random()*100)+1;

//input.addEventListener('keydown', function(e) {
//    console.log(e);
//})

// check your guessed number

var remainingAttempts = 7;

function checkNum() {
    if (remainingAttempts <= 0) {
        info.textContent='Press Start Over to play again.';
        info.style.color='blue';
        
        return;
    }
    
    if (!(input.value >= 1 && input.value <= 100)) {
        info.textContent='Enter a number between 1 and 100!';
        info.style.color='red';
        
        return;
    }
    
     if (input.value > rand) {
        remainingAttempts-=1;
        info.textContent='Guess lower. Attempts left: ' + remainingAttempts;
         info.style='black';
    } else if (input.value < rand) {
        remainingAttempts-=1;
        info.textContent='Guess higher. Attempts left: ' + remainingAttempts;
         info.style='black';
    } else if (input.value == rand) {
        info.textContent='Correct! The number was: ' + rand;
        info.style.color='green';
        
        function getRandomColors() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            var color2 = '#';
            for (var i = 0; i < 6; i++) {
                let rand = Math.floor(Math.random() * 16);
                color += letters[rand];
                color2 += letters[15 - rand];
            }
            
            return [color, color2];
        }
        
        var randomColors = getRandomColors();
        h1.style.color= randomColors[0];
        h1.style.backgroundColor = randomColors[1];
    }
    
    if (remainingAttempts <= 0) { 
        info.textContent='You have lost. The number was: ' + rand;
        info.style.color='red';
    }
    
    input.value=null;
    input.focus();
}

// click event
guess.addEventListener('click', function() {
    checkNum();
})

startOver.addEventListener('click', function() {
    remainingAttempts = 7;
    rand = Math.floor(Math.random()*100)+1;
    info.textContent='You only have 7 attempts.'
    info.style.color='black';
    
})

// keydown event
input.addEventListener('keydown', function(event) {
    if (event.keyCode == 13) {
        checkNum();
    }
})