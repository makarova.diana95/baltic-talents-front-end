// Funkcijos

function addNumbers(a, b) {
    let sandauga = a * b;
    //    console.log(`sandauga yra: ${sandauga}`);
}

addNumbers(10, 10);

// Kintamuju terpes

//var i = 0;
//var x;

function remake() {
    i = 1;
    x = 1;
}

remake();

function getArea(a, b) {
    const area = a * b;
    if (area > 50) console.log('plotas:', area);
}

function getPerimeter(c, d) {
    const perimeter = (c + d) * 2;
    console.log('perimetras:', perimeter);
}

var shape1 = [5, 9];

//getArea(shape1[0], shape1[1]);
//getPerimeter(shape1[0], shape1[1]);

var allShapes = [[4, 8], [3, 7], [9, 8], [5, 7]];

for (i = 0; i < allShapes.length; i++) {
    //    getArea(allShapes[i][0], allShapes[i][1]);
    //    getPerimeter(allShapes[i][0], allShapes[i][0]);
}

var preke = ['suris', 'zemaitijos pienas', 0.34, 3.25, 2011];
var preke1 = ['pienas', 'zemaitijos pienas', 1, 1.5, 2015];

// Prekiu parduotuve

function getName(p) {
    console.log(p[0]);
}

function getManufacturer(p) {
    console.log(p[1]);
}

function getWeight(p, unit) {
    const weight = p[2] * unit;
    return weight; // geriau return p[2] * unit; 
}

function checkOut(p, cash, unit, weight) {
    const total = p[3] * unit;
    const change = cash - total;
    console.log('graza:', change, `pirkta: ${weight}kg`);
}

function availableSince(p, now) {
    console.log(now - p[4]);
}

function init(product, unit, cash) {
    getName(product);
    getManufacturer(product);
    let productWeight = getWeight(product, unit);
    checkOut(product, cash, unit, productWeight);
    availableSince(product, 2017);
}

//init(preke, 5, 50);
//console.log('--------------------');
//init(preke1, 3, 10);

// #1 Function statement

function getSum(a, b) {
    console.log(a + b);
}

// #2 Function expression

let getMulti = function (a, b) {
    return a * b;
}

// #3 Function expression ES6

let getTotals = (argumentas) => {
    console.log(argumentas);
}

// Callbacks

var birthDate = 1999;

function getAge(birth) {
    console.log(2017 - birth);
}

function getInfo(callback, year) {
    callback(year);
}

getInfo(getAge, birthDate);

console.log('--------------------');

var player = ['Torres', 47, 9, 6, 5];
var player2 = ['Ronaldo', 11, 12, 6, 2];

function playerAccuracy(player) {
    let hits = player[1];
    let misses = player[2];
    let totalAttempts = hits + misses;

    return 100.0 * hits / totalAttempts;
}

function goalsPerMatch(player) {
    let goals = player[1];
    let totalMatches = player[3];

    return goals / totalMatches;
}

function playerAggression(player) {
    let cards = player[4];
    let totalMatches = player[3];

    return cards / totalMatches;
}

function playerStats(player) {
    let accuracy = playerAccuracy(player);
    let goals = goalsPerMatch(player);
    let cardsPerGame = playerAggression(player);
    console.log(`Player statistics for ${player[0]}:`);
    console.log(`Accuracy: ${accuracy.toFixed(2)}%`);
    console.log(`Goals per match: ${goals.toFixed(2)}`);
    console.log(`Aggression: ${cardsPerGame.toFixed(2)} cards per game`);

    return [accuracy, goals, cardsPerGame];
}

playerStats(player);
console.log('-------')
playerStats(player2);

//function comparePlayerStats(playerA, playerB) {
//    playerAStats = playerStats(playerA);
//    playerBStats = playerStats(playerB);
//    let statisticType;
//    for (let i = 0; i < playerAStats.length; i++) {
//        if (i == 0) {
//            statisticType ='Accuracy';
//        }
//
//        if (i == 1) {
//            statisticType = 'Goals per match';
//        }
//
//        if (i == 2) {
//            statisticType ='Cards per game';
//        }
//        
//        if (playerAStats[i] > playerBStats[i]) {
//            console.log(playerA[0] + ' has higher ' + statisticType + ' than ' + playerB[0]);
//        } else if (playerAStats[i] < playerBStats[i]) {
//            console.log(playerB[0] + ' has higher ' + statisticType + ' than ' + playerA[0]);
//        } else {
//            console.log(playerA[0] + ' has the same ' + statisticType + ' as ' + playerB[0]);
//        }
//    }
//}
//
//comparePlayerStats(player, player2);
