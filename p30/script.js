// Kintamieji
const counter = document.querySelector('h2');
const person = document.querySelector('input[type=text]');
const amount = document.querySelector('input[type=number]');
const add = document.querySelector('button');
const list = document.querySelector('ul');

person.addEventListener('click', function() {
    person.value = '';
})

let listData = [];

function getTotal() {
    let total=0;
    listData.forEach(item=>{
        total+=item.amount;
    })
    counter.textContent=total;
}

add.addEventListener('click', function() {
    var id = _.uniqueId();
    
    const html = `<li data-id="${id}">${person.value} $${amount.value} <span>X</span></li>`;
    list.insertAdjacentHTML('afterbegin', html);
    
    listData.push({
        id,
        person:person.value,
        amount:parseFloat(amount.value)
    })
    
    getTotal();
    console.log(listData);
    
    person.value='';
    amount.value='';
    
})

list.addEventListener('click', function(e) {
    console.log(e.target);
    if (!e.target.matches('span')) return;
    list.removeChild(e.target.parentNode);
    var remove = e.target.parentNode.dataset.id;
    
    var sorted = listData.filter(item=>item.id!=remove)
    // jei funkcija daro tik return ir yra vienos eilutes ^^^ ir neberasom return
    listData=sorted;
    getTotal();
    console.log(listData);
    
})

//var numbers = [1,2,3,4,5,6];
//var sorted = numbers.filter(item=>{
//    return item>3;
//})
//
//console.log(sorted);