const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');

gulp.task('test', function () {
    console.log('gulp is running')
});

gulp.task('compile', function () {
    return gulp.src('./sass//*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({debug: true}, function (details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
        .pipe(gulp.dest('./css'));
});

gulp.task('imagemin', () =>
    gulp.src('./img//*')
        .pipe(imagemin())
        .pipe(gulp.dest('./img'))
);

gulp.task('sass:watch', function () {
    gulp.watch('./sass//*.scss', ['compile']);
    gulp.watch('./img//*', ['imagemin']);
});

gulp.task('default', ['sass:watch']);
