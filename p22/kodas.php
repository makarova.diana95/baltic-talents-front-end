<html>

<head>
    <meta charset="UTF-8">
    <title>P22 JS</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>

    <h1>Parduotuvė</h1>
    <div class="wrapper">
        <form class="shopForm" method="get">
            <h3>Prekė</h3>
            <input required type="text" name="item">
            <h3>Svoris</h3>
            <input required type="number" name="weight" min="1">
            <h3>Kiekis</h3>
            <input required type="number" name="quantity" min="1">
            <h3>Kaina</h3>
            <input required type="number" name="price" min="0.01" step="0.01">
            <input type="submit" value="Į krepšelį">
        </form>

        <h3>Viso:</h3>
        <h3 class="total">--</h3>
        
        <form class="checkoutForm" method="post" action="checkout.php">
            <input type="hidden" value="0.0" name="totalPrice">
            <input type="submit" value="Apmokėti" disabled>
        </form>
        
        <?php session_start(); if ($_SESSION['totalPrice'] && (float) $_SESSION['totalPrice'] > 0) { ?>
        <div class="success">Jūs apmokėjote <?php echo $_SESSION['totalPrice']; ?>&euro;</div>
        <?php unset($_SESSION['totalPrice']); } ?>
    </div>

    <script src="script.js"></script>
</body>

</html>
