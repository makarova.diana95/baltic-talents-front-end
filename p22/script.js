console.log('Objektai');

// properties (savybes);
// methods (metodai);
// visi metodai yra funkcijos

const person = {
    name: 'Jonas',
    surname: 'Jonaitis',
    dateOfBirth: 1997,
    status: 'Student',
    mathGrades: [10, 2, 5, 8, 9],
    job: {
        type: 'Barmenas',
        salary: 344,
    },
    familyMembers: [
        {
            status: 'Mother',
            name: 'Juzefa',
        },
        {
            status: 'Father',
            name: 'Antanas',
        }
    ],
    getAge: function(currentDate) {
        return currentDate - this.dateOfBirth;
    },
    // metodas pagal ES6 sintakse
    getInfo() {
        console.log(this.name, this.surname);
    },
    getAvg() {
        let sum = 0;
        // sena sintakse:
        //        person.mathGrades.forEach(function (grade) {
        //            sum+=grade;
        //        })

        // ES6:
        // jeigu ES6 anonimine fn turi tik viena argumenta tai mums nereikia skliausteliu
        //    person.mathGrades.forEach(grade => {
        //    })
        //    

        // jei vykd blokas tik viena eilute mums nereikia vykd bloko skliaustu
        this.mathGrades.forEach(grade => sum += grade)
        return sum/person.mathGrades.length;
    },
    getFamilyMembers() {
        this.familyMembers.forEach(member=>{
            console.log(member.name);
        })
    }
}

console.log(person.mathGrades[0]);
// iskvieti metoda
console.log(person.getAge(2017));
person.getInfo();

console.log(person.getAvg());

person.getFamilyMembers();

console.log('----------------');
console.log('----------------');

// Prekiu krepselis

// Kintamieji

const itemName = document.querySelector('.shopForm input[name="item"]');
const itemWeight = document.querySelector('.shopForm input[name="weight"]');
const itemQuantity = document.querySelector('.shopForm input[name="quantity"]');
const itemPrice = document.querySelector('.shopForm input[name="price"]');
const total = document.querySelector('.total');
const shopForm = document.querySelector('.shopForm');
const checkoutForm = document.querySelector('.checkoutForm');
const totalPrice = document.querySelector('.checkoutForm input[name="totalPrice"]');
const checkoutSubmit = document.querySelector('.checkoutForm input[type="submit"]');
const checkoutSuccess = document.querySelector('.wrapper .success');
// Kintamasis kuris saugo visas prekes

let itemsList = [];
let sum = 0;
itemName.focus();

shopForm.addEventListener('submit', function(e) {
    e.preventDefault();
    // sukuriam objekta kurio properties bus inputu reiksmes
    // parseInt() paima be kablelio arba parseFloat() su kableliu vercia stringa i sk
    const item = {
        name: itemName.value,
        weight: parseFloat(itemWeight.value),
        quantity: parseInt(itemQuantity.value),
        price: parseFloat(itemPrice.value),
    }
    // idedam si objekta i itemsList masyva
    itemsList.push(item);
    sum+=item.price*item.quantity;
    total.textContent=`${sum}€`;
    totalPrice.value = sum;
    
    checkoutSubmit.disabled = true;
    if (totalPrice.value > 0) {
        checkoutSubmit.disabled = false;
    }
    
    itemName.value=null;
    itemWeight.value=null;
    itemQuantity.value=null;
    itemPrice.value=null;
    itemName.focus();
})

// padaryti checkout mygtuka
// sukurti objekta futbolo zaidejas, kuris turi tureti savo varda, pavarde, amziu, numeri, zaistu rungtyniu skaiciu, bendra pelnytu ivarciu kieki
// metodai: avg tasku skaicius per rungtynes
// gauti zaidejo informacija
// su salyga palyginti dvieju zaideju rezultatus

const player = {
    name: 'Lionel',
    surname: 'Messi',
    age: 30,
    number: 10,
    playedMatches: 390,
    totalGoals: 360, 
    getAvgGoalsPerMatch() {
       return parseFloat(this.totalGoals/this.playedMatches).toFixed(2);
    },
    getInfo() {
        console.log(this.name, this.surname);
        console.log(this.age);
        console.log('Average goals per match: ' + this.getAvgGoalsPerMatch());
    }
}

//console.log(player.getInfo());