console.log('ciklai');

var prekes = [['kumpis', 10], ['zuvis', 12]]

console.log(prekes[0][0]);
console.log(prekes[0][1] + prekes[1][1]);

console.log('----------------------')

var arr = [[3, 5, 9], [5, 10, 11, 5], [2, 7, 5, 6]];
console.log('paskutinis', arr[arr.length - 1]);

for (let x = 0; x < 4; x++) {
    console.log(arr[0][x]);
    console.log(arr[1][x]);
    console.log(arr[2][x]);
}

console.log('----------------------')

for (let i = 0; i < arr.length; i++) {
    for (let x = 0; x < arr[i].length; x++) {
        console.log(arr[i][x]);
        //        console.warn(i, '-', x);
    }
}

console.log('----------------------')
console.log('----------------------')
console.log('----------------------')

// isrikiuoti masyvo narius mazejancia tvarka
var a = [45, 23, 11, 5, 6, 27, 50, 41, 35, 20];
var sorted = [];
var aLength = a.length;

for (i = 0; i < aLength; i++) {
    var max = 0;
    for (let x = 0; x < a.length; x++) {
        if (a[x] > max) {
            max = a[x];
        }
    }
    sorted.push(max);
    let maxIndex = a.indexOf(max);
    a.splice(maxIndex, 1);
}
a = sorted;

// bet koks dvigubo lygio masyvas arba sis
// var nr = [[13,6,8], [3,4,6,2,5,7,9], [2,4,6,23]]

// isprintuoti visus masyvo skaicius i konsole
// surasti masyvo nariu suma
// surasti didziausia elementa
// i konsole atvaizduoti visus skaicius is masyvo kurie dalijasi is 2
// surasti nr masyvo bloku sumas (siuo atveju tris atskiras sumas)

var nr = [[13,6,8], [3,4,6,2,5,7,9], [2,4,6,23]];
var maxNr = 0;
for (i = 0; i < nr.length; i++) {
    var blockSum = 0;
    for (j = 0; j < nr[i].length; j++) {
        console.log(nr[i][j]);
        if (nr[i][j] > maxNr) {
            maxNr = nr[i][j];
        }
        
        if (nr[i][j] % 2 == 0) {
            console.log('dalijasi is 2:', nr[i][j]);
        }
        
        blockSum += nr[i][j];
    }
    console.log('bloko suma:', blockSum);
    console.log('----------------------');
}
