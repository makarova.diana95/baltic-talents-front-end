const latitude = document.querySelector('.lat');
const longitude = document.querySelector('.lng');
const search = document.querySelector('.coordinates button');
const addressInput = document.querySelector('.address input');
const findLocation = document.querySelector('.address button');

function initMap(lat=0, lng=0) {
    var uluru = {lat, lng};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: uluru
    });
    var marker = new google.maps.Marker({
      position: uluru,
      map: map
    });
}

search.addEventListener('click', function() {
    const latValue = parseFloat(latitude.value);
    const lngValue = parseFloat(longitude.value);
    console.log(latValue, lngValue);
    initMap(latValue, lngValue);
})

// get JSON su jQuery
// $.getJSON('http://...', function(data){})

findLocation.addEventListener('click', function() {
    let address = addressInput.value;
    let url = `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=AIzaSyDb-xOAGGCcnyDZ1L92U1d-h9oOLP8A4_o`;
    $.getJSON(url, function(data) {
        let lat = data.results[0].geometry.location.lat;
        let lng = data.results[0].geometry.location.lng;
        
        console.log(lat, lng);
        initMap(lat, lng);
    })
})

// Currencies
const currencyList = document.querySelector('ul');
const findCurrency = document.querySelector('.find-currency');

findCurrency.addEventListener('click', function() {
    let url = 'http://api.fixer.io/latest';
    $.getJSON(url, function(data) {
        let currency = data.rates;
        for (item in currency) {
            console.log(item, currency[item]);
            const html = `<li>Currency: ${item}, rate: ${currency[item]}</li>`
            currencyList.insertAdjacentHTML('beforeend', html)
        }
    })
})

$.getJSON('http://kursai.local/p29/posts.json', function(data) {
    console.log(data);
})