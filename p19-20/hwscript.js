
const input = document.querySelector('.converter input');
const convertedResult = document.querySelector('.converted_result');
const eurToUsdRate = 1.19;
const usdToEurRate = 1 / 1.19;

input.addEventListener('keyup', function() {
    convertedResult.textContent=(input.value * eurToUsdRate).toFixed(2);
})


