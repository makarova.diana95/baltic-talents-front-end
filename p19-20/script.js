// kintamieji
const btn = document.querySelector('.fa-bars');
const menu = document.querySelector('header ul');
const header = document.querySelector('header');
const mainImg = document.querySelector('.gallery img');
const left = document.querySelector('.gallery .fa-chevron-left');
const right = document.querySelector('.gallery .fa-chevron-right');
const circle = document.querySelectorAll('.circle');

const images = ['img/react-logo.png', 'img/logo.png', 'img/javascript1600.png'];
const title = document.querySelector('h2');

const articles = document.querySelectorAll('.article');
const pictures = document.querySelectorAll('.picture');

const menuBox = document.querySelector('.box');

btn.addEventListener('click', function() {
    menu.classList.toggle('active');
})

let current = 0;
circle[current].style.backgroundColor='rebeccapurple';

right.addEventListener('click', function() {
    circle[current].style.backgroundColor='white';
    current++;
    
    if (current == images.length) {
        current = 0;
    }
    
    mainImg.src=images[current];
    circle[current].style.backgroundColor='rebeccapurple';
})

left.addEventListener('click', function() {
    circle[current].style.backgroundColor='white';
    current--;
    
    if (current == -1) {
        current = images.length - 1;
    }
    
    mainImg.src=images[current];
    circle[current].style.backgroundColor='rebeccapurple';
})

let start = window.scrollY;

window.addEventListener('scroll', function() {
    // animuojam meniu
    if (window.scrollY > title.offsetTop) {
        header.style.backgroundColor='rgba(255,0,0,.7)';
    } else {
        header.style.backgroundColor='';
    }
    
    // animuojam teksta
    for (let i = 0; i < articles.length; i++) {
        if (window.scrollY + window.innerHeight * .9 > articles[i].offsetTop) {
            articles[i].style.opacity=1;
        }
    }
        
    // animuojam paveikslelius
    for (let i = 0; i < pictures.length; i++) {
        if (window.scrollY + window.innerHeight * .9 > pictures[i].offsetTop) {
            pictures[i].style.opacity=1;
            pictures[i].style.transform='translateX(0)';
        }
    }      
    
    // animuojam sumazejanti meniu
    if (window.scrollY < start) {
//        console.log(9544);
        header.style.height='';
    } else {
        header.style.height='0px';
    }
    
    start = window.scrollY;
    
})



