const App = (function () {

    const DOMstrings = {
        menu: document.querySelector('.menu ul'),
        menuItems: null,
        table: document.querySelector('.table'),
        tableTotal: document.querySelector('.tables .table .total')
    }
    
    const menuData = [];

    let tableData = [];
    
    let tableTotal = 0;
    
    function populateMenu() {
        var url = 'http://kursai.local/p31-32/dist/script/menu.json';
        $.getJSON(url, function (data) {

            data.menu.forEach(item => {
                const html = `<li>${item.name} <span>$${item.price}</span></li>`;
                DOMstrings.menu.insertAdjacentHTML('beforeend', html);
                menuData.push(item);
                console.log(item);
            })
            // surenkam visus meniu itemus i sarasa
            DOMstrings.menuItems=document.querySelectorAll('.menu li');
            console.log(DOMstrings.menuItems);
//            console.log(menuData);
            // Uzdedam event listenerius and meniu itemu
            addEvents();
        })
    }
    
    function addEvents() {
        DOMstrings.menuItems.forEach((item, i)=>{
            item.addEventListener('click', function () {
                console.log(i);
                addOrder(i);
            })
        })
        DOMstrings.table.addEventListener('click', function (e) {
            if(e.target.matches('span') && e.target.classList.contains('remove')) {
                DOMstrings.table.removeChild(e.target.parentNode);
                const orderId = e.target.parentNode.dataset.id;
                const sorted = tableData.filter(item => item.id != orderId);
                tableData=sorted;
                getTotal();
            }
        })
    }
    
    function addOrder(index) {
        const id = _.uniqueId();
        const html = `<li data-id="${id}">
                        <span class="food">${menuData[index].name}</span>
                        <span class="price">$${menuData[index].price}</span>
                        <span class="remove">X</span>
                    </li>`;
        DOMstrings.table.insertAdjacentHTML('beforeend', html)
        const obj = {...menuData[index], id}; // ... <- spread operatorius (nukopijuoja objekto props, methods)
        tableData.push(obj);
        
        console.log(tableData);
        getTotal();
    }
    
    function getTotal() {
        let tableTotal = 0;
        tableData.forEach(item=>tableTotal+=item.price);
        console.log(tableTotal);
        DOMstrings.tableTotal.textContent=`$${tableTotal}`;
    }

    function showMenu() {
        $('.menu h3').click(function () {
            $('.menu ul').slideToggle();
        })
    }

    console.log('App is up and running');

    return {
        init: function () {
            populateMenu();
            showMenu();
        }
    }
})()

App.init();
