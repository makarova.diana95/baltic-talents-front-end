$(document).ready(function() {
    console.log('jquery');
    
    $('h1').css('color', 'red');
    
    $('ul li').css('font-size', '24px');
    
    $('li').click(function() {
        $(this).css('color', 'red');
    })
    
    $('.menu span').click(function() {
        $('ul').slideToggle();
    })
    
    for(i=0; i<10; i++) {
        for(j=0; j<10; j++) {
            $('.container').append(`<div class="box" style="top:${i*50}px; left:${j*50}px"></div>`);
        }
    }
    
    $('.box').mouseenter(function() {
        $(this).css('background', 'black').fadeOut();
    })
    
    $('button').click(function() {
        console.log(_.uniqueId());
    })
    
    for (let k=0; k<4; k++) {
        $('.list').append(`<p data-id="${_.uniqueId()}">element ${k}</p>`)
    }
    
    $('p').click(function() {
        console.log($(this).data('id'));
        $(this).empty();
    })
})