// loginiai operatoriai
// && ir
// || arba

/*var no = Math.floor(Math.random() * 30)
if (no >= 10 && no <= 20) {
    console.log(`skaicius ${no} yra tarp 10 ir 20`);
} else {
    console.log(`skaicius ${no} nera tarp 10 ir 20`);
}

if (no <= 10 || no >= 20) {
    console.warn(no)
} else {
    console.warn(`${no}, pralaimejote`)
}*/

// ciklai
/*var num = 0;
while (num < 10) {
    num += 2
    if (num == 6) continue
    console.log(num)
}

var rand = Math.floor(Math.random() * 100) + 1

var n = 0;
while (n!= rand) {
    n++
    console.log(n)
    if (n == rand) {
        console.error(rand)
    }
}*/

/*for (let x = 0; x < 10; x++) {
    let num =  Math.floor(Math.random() * 100) + 1
    if (num > 70) console.log(num)
}

for (let x = 0; x < 100; x++) {
    let num =  Math.floor(Math.random() * 100) + 1
    console.log(num)
}*/

let a = [2, 4, 7, 'preke', 5, 9, 2, 6, 11, 8, 22, 9]
console.log(a[a.length - 1])

let prekes = ['obuoliai', 'pomidorai', 'suris', 'varske']

// masyvo metodai
// .pop() istrina pirma el
prekes.pop()
// .push('element') ideda el i masyvo pabaiga
prekes.push('pienas')
// .splice(index, kiek nariu trinti, optional(pakeisti i))
prekes.splice(0, 1, 'arbuzas')
// indexOf(masyvo elementas)
var i = prekes.indexOf('suris')
// A.concat(B) sujungti masyvus
var newArr = prekes.concat(a)
// .replace (ka pakeisti, i ka pakeisti) 

var numbers = [3,5,7,10,8,6,7,7]
//console.log(numbers[0] * numbers[1])
for (let x = 0; x < numbers.length; x++) {
    console.log(numbers[x]);
}

console.log('-----------------------')

for (let x = 0; x < 4; x++) {
    console.log('Hello');
}

console.log('-----------------------')

for (let x = 0; x < 10; x++) {
    console.log(Math.floor(Math.random() * (50 - 10) + 10));
}

console.log('-----------------------')

let num = 4;

for (let x = 0; x < 10; x++) {
    num += 1;
    console.log(num);
}

console.log('-----------------------')

for (let x = 0; x < 10; x+=2) {
    console.log(x);
}

console.log('-----------------------')

for (let x = 0; x < 100; x++) {
    if (x % 3 == 0 && x % 5 == 0) {
        console.log('XY');
    } else if (x % 3 == 0) {
        console.log('X');
    } else if (x % 5 == 0) {
        console.log('Y');
    } else {
        console.log(x);
    }
}

console.log('-----------------------')

let arr = [1,2,3,4,5,6,7,8,9,10]

for (let x = 0; x < arr.length; x++) {
    console.log(arr[x]);
}

console.log('-----------------------')

for (let x = 0; x < arr.length; x+=2) {
    console.log(arr[x]);
}

console.log('-----------------------')

var randomNumbers = [];

for (let x = 0; x < 10; x++) {
    randomNumbers.push(Math.floor(Math.random() * 100));
}
console.log(randomNumbers);

console.log('-----------------------')

var randomNumbersSum = 0;
for (x = 0; x < randomNumbers.length; x++) {
    randomNumbersSum += randomNumbers[x];
}
console.log(randomNumbersSum);

console.log('-----------------------')

var avg = randomNumbersSum / randomNumbers.length;
console.log(avg);

console.log('-----------------------')

maxNum = 0;
for (let x = 0; x < randomNumbers.length; x++) {
    if (randomNumbers[x] > maxNum) {
        maxNum = randomNumbers[x];
    }
}
console.log(maxNum);
