let btnA = document.querySelector('.a');
let btnB = document.querySelector('.b');
let btnC = document.querySelector('.c');
let btnD = document.querySelector('.d');

let wrapper = document.querySelector('.wrapper');

btnA.addEventListener('click', function() {
    const html = `<span>A</span>`;
    wrapper.insertAdjacentHTML('beforebegin', html)
})

btnB.addEventListener('click', function() {
    const html = `<span>B</span>`;
    wrapper.insertAdjacentHTML('afterbegin', html)
})

btnC.addEventListener('click', function() {
    const html = `<span>C</span>`;
    wrapper.insertAdjacentHTML('beforeend', html)
})

btnD.addEventListener('click', function() {
    const html = `<span>D</span>`;
    wrapper.insertAdjacentHTML('afterend', html)
})