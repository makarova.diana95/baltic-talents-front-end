console.log('Racing game');

// kintamuju nuemimas
const driver = document.querySelector('input[type=text]');
const speed = document.querySelector('input[type=number]');
const add = document.querySelector('.add');
const race = document.querySelector('.race');
const list = document.querySelector('.list');
const track = document.querySelector('.race_track');

let carList = [];

// masinos konstruktorius
function Car(carName, carSpeed) {
    this.carName = carName;
    this.carSpeed = parseFloat(carSpeed);
    this.addCarToList = function() {
        const html = `<li>${this.carName}</li>`;
        list.insertAdjacentHTML('beforeend', html);
    };
    this.addCarToTrack = function() {
        const rand = Math.floor(Math.random()*4+1);
        const sp = 1/this.carSpeed*100;
        const html = `<img src="img/car${rand}.png" style="transition: ${sp}s">`;
        track.insertAdjacentHTML('beforeend', html);
    }
}

// idedam masina i sarasa
add.addEventListener('click', function() {
    let car = new Car(driver.value, speed.value);
    carList.push(car);
    console.log(carList);
    carList[carList.length-1].addCarToList();
    carList[carList.length-1].addCarToTrack();
    driver.value=null;
    speed.value=null;
    driver.focus();
})

race.addEventListener('click', function() {
    let allCars = document.querySelectorAll('.race_track img');
    allCars.forEach(car=>{
        car.style.marginLeft='90%';
    })
    let max = 0;
    let bestDriver = 0;
    carList.forEach((car, i)=>{
        if (car.carSpeed > max) {
            max = car.carSpeed;
            bestDriver = i;
        }
    })
    console.log(max, bestDriver);
    console.log(carList[bestDriver]);
    
    const bestTime = 1/carList[bestDriver].carSpeed*100*1000;
    
    setTimeout(()=>{
        console.warn(`Fastest car: ${carList[bestDriver].carName}, ${carList[bestDriver].carSpeed}`);
    }, bestTime)
})


//#1 innerHTML
//#2 appendChild()
//#3 insertAdjacentHTML


// padaryti jei daugiau negu 10 raidziu parasyti jas ir prideti daugtaski...