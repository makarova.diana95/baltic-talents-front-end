<!doctype html>
<html lang="en">
    <?php include 'parts/head.php'?>
<body>

<section class="home-landing">
    <header class="container">
        <nav>
            <ul>
                <li><a href="">Home</a></li>
                <li><a href="">Portfolio</a></li>
                <li><a href="">Blog</a></li>
                <li class="center">
                    <img src="" alt="">
                    <span>Expire</span>
                </li>
                <li><a href="">Home Pages</a></li>
                <li><a href="">About Us</a></li>
                <li><a href="">Contact Us</a></li>
            </ul>
        </nav>
    </header>
    <div class="wrapper">
        <i class="fa fa-chevron-left" aria-hiden="true"></i>
        <i class="fa fa-chevron-right" aria-hidden="true"></i>

        <h1>Expire</h1>
        <h2>Professionally designed, carefully made for your enjoyment</h2>
        <button>explore</button>
        <button>learn more</button>
    </div>
</section>



<section class="home-about">
<div class="corner"></div>

    <div class="container">
        <h2>About Us</h2>
        <div class="line"></div>
        <h4>This is who we are - or at least who we strive to be...</h4>
        <div class="wrapper">
            <div class="info">
                <h3>If you can't explain it simply, you don't understand it well enough.</h3>
                <button>The more you know <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
            </div>

            <div class="box">
                <div class="fold"></div>
                <img src="./img/service1.png">
                <h3>Typography</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin non elit dui. Nunc pharetra, lacus sed aliquet lobortis, dolor mauris semper velit, nec tincidunt tellus ex nec ex. Suspendisse potenti.</p>
            </div>
            <div class="box">
                <img src="./img/service2.png">
                <h3>Full Icon Set</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin non elit dui. Nunc pharetra, lacus sed aliquet lobortis, dolor mauris semper velit, nec tincidunt tellus ex nec ex. Suspendisse potenti.</p>
            </div>
            <div class="box">
                <img src="./img/service3.png">
                <h3>Accurate</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin non elit dui. Nunc pharetra, lacus sed aliquet lobortis, dolor mauris semper velit, nec tincidunt tellus ex nec ex. Suspendisse potenti.</p>
            </div>
        </div>
    </div>

</section>

<section class="home-paralax-red">
    <div class="corner"></div>
    <div class="container">
        <h2>Paralax Section</h2>
        <div class="line"></div>
        <h4>Exploring the unexplored is a risky business, but a rewarding one.</h4>
        <p><span>“</span>
        Design is in everything we make, but it's also between those things. It's a mix of craft, science, storytelling, propaganda, and philosophy.</p>
        <h3>— Erik Adigard</h3>
    </container>
</section>
<section class="home-paralax-grey">
    <div class="container">
        <div class="wrapper">
        <img src="./img/user.png">
            <h3>1000+
            </h3>
            <h4>Happy users</h4>
        </div>
        <div class="wrapper">
        <img src="./img/product.png">
            <h3>
            200
            </h3>
            <h4>Sold products</h4>
        </div>
        <div class="wrapper">

        <img src="./img/comment.png">
            <h3>
            5632
            </h3>
            <h4>Comments</h4>
        </div>
        <div class="wrapper">

        <img src="./img/code.png">
            <h3>
            25000+
            </h3>
            <h4>Lines of code</h4>
        </div>
    </div>
</section>

    <?php include 'parts/footer.php'?>
</body>
</html>
